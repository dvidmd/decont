##########################################################
###### Script Para Descargar Versión David 20-12-24 ######
################ Actualizado: 21-01-21 ###################
##########################################################

# INDICACIONES TOMÁS:

# This script should download the file specified in the first argument ($1),
# place it in the directory specified in the second argument ($2), and
# *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes":

# COMENTARIOS MÍOS:

# $1 son los archivos que hay que descargar
# $2 es el directorio donde hay que introducir los archivos descargados
# $3 es el sampleid, cuya condición indica si se descomprime si es == yes o no lo hace

archivos_descargar=$1
directorio_destino=$2
condicional_descompresion=$3

## DESCARGAR:

mkdir -p $directorio_destino

echo "Descargando archivos:"
echo " "
wget -P $directorio_destino $archivos_descargar

## DESCOMPRIMIR:

nombre_archivo=$(basename "$archivos_descargar")
direccion_archivo=$(echo "$directorio_destino/$nombre_archivo")

if [ "$condicional_descompresion" == "yes" ];
then
	gunzip -k $direccion_archivo
fi

##########################################################
