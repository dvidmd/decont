###########################################################
###### Script Ejercicio extra Versión David 20-12-24 ######
################ Actualizado: 21-01-21 ####################
###########################################################

## Replace the loop that downloads the samples data files with a wget one-liner:

## Copiar contenido del script pipeline.sh modificando unicamente el inicio, escribiendo wget -i ... en lugar del bucle for de descarga.

###############################################

echo " "
echo "###############################################"
echo "######## Programa de descontaminación #########"
echo "###############################################"
echo " "
echo "Proceso de descontaminación de muestras iniciado..."
echo " "

###############################################
### DIFERENCIA RESPECTO A PIPELINE.SH #########
###############################################

wget -nc -i data/urls -P data

###############################################
###############################################

# Se crean los directorios necesarios:

mkdir -p out/merged
mkdir -p out/trimmed
mkdir -p res/contaminants_idx
mkdir -p log/cutadapt
mkdir -p out/star

###########################################################
###########################################################

# Y se borra lo siguiente (debido a que es redundante):

# Download all the files specified in data/filenames:

#for url in $(cat data/urls) #TO DO
#do
#    bash scripts/download.sh $url data
#done

echo "Genoma descargado."
echo " "

###########################################################

# Download the contaminants fasta file, and uncompress it:

echo "Descargando fichero del genoma con contaminantes:"
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes #TO DO
echo "Fichero del genoma con contaminantes descargado."
echo " "

## Debería descomprimise desde el propio script download.sh

# Index the contaminants file:

echo "Indexación del genoma de contaminantes:"
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx
echo " "
echo "Indexación completada."
echo " "

# Merge the samples into a single file:

echo "Realizando unión de archivos:"
echo " "

list_of_sample_ids=$(ls data/*.fastq.gz | cut -d "-" -f1 | sort | uniq)

for muestra in ${list_of_sample_ids} #TODO
do
        sid=$(basename ${muestra})
        bash scripts/merge_fastqs.sh data out/merged $sid
        echo "==========> Muestra $sid unida."
        echo " "
done

echo "Unión realizada."
echo " "

# TO DO: run cutadapt for all merged files (se puede utilizar la siguiente línea pero modificándola):

echo "Realizando cutadapt para eliminar adaptadores:  "

for muestra in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sort | uniq) # TO DO
do
        echo " "
        sampleid=$(basename ${muestra})
        echo "Corriendo cutadapt para la muestra ${sampleid} :"
        cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sampleid}.trimmed.fastq.gz out/merged/${sampleid}.fastq.gz > log/cutadapt/${sampleid}.log
done

echo " "
echo "Eliminación de adaptadores completada."

# TO DO: run STAR for all trimmed files:

echo " "
echo "Realizando alineamiento:   "

for fname in out/trimmed/*.fastq.gz
do

# you will need to obtain the sample ID from the filename:

        echo " "
        sampleid=$(basename $fname | cut -d "." -f1)
        echo "Corriendo STAR para la muestra ${sampleid} :"
        mkdir -p out/star/$sampleid
        STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/$sampleid.trimmed.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix out/star/${sampleid}/
done

############################################################################################

## CREAR LOG:

# TO DO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run):

# - cutadapt: Reads with adapters and total basepairs:

nombres=$(ls log/cutadapt/*.log | cut -f 1 -d '.')

for nombre_largo in $nombres
do
        nombre_log=$(basename $nombre_largo)
        echo " " >> Log.out
        echo "##############################################################" >> Log.out
        echo "Información relacionada con $nombre_log según:" >> Log.out
        echo " " >> Log.out
        echo "cutadapt ==>" >> Log.out
        grep "Reads with adapter" log/cutadapt/${nombre_log}.log >> Log.out
        grep "Total basepairs processed" log/cutadapt/${nombre_log}.log >> Log.out
        echo " " >> Log.out
        echo "STAR ==>" >> Log.out
        grep "Uniquely mapped reads %" out/star/${nombre_log}/Log.final.out >> Log.out
        grep "% of reads mapped to multiple loci" out/star/${nombre_log}/Log.final.out >> Log.out
        grep "% of reads mapped to too many loci" out/star/${nombre_log}/Log.final.out >> Log.out
        echo " "
        echo "##############################################################" >> Log.out
done

echo " "

# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in (¡IMPORTANTE!)

##########################################################

echo "Alineamiento completado para todas las muestras."
echo " "
echo "Programa finalizado. Los resultados se pueden observar en el Log.out"
echo " "
echo "###############################################"
echo "########### LOG.OUT ==> RESULTADOS ############"
echo "###############################################"

cat Log.out
echo " "

############################################################
