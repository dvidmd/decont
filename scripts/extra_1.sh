#################################################
###### Script Extra David Versión 20-12-24 ######
############ Actualizado: 21-01-22 ##############
#################################################

## Check if the output already exists before running a command.
## If it exists, display a message and continue.

## Copio el script pipeline.sh pero modificándolo, utilizando if/else
## en cada paso para comprobar en cada bloque si ya existe/se ha producido:

# Comentarios Tomás: Dentro del ejercicio:

echo " "
echo "###############################################"
echo "######## Programa de descontaminación #########"
echo "###############################################"
echo " "
echo "Proceso de descontaminación de muestras iniciado..."
echo " "

# Se crean los directorios necesarios (salvo res/contaminants_inx, el cual generaría un problema con su condicional if/else):

mkdir -p out/merged
mkdir -p out/trimmed
mkdir -p log/cutadapt
mkdir -p out/star

# Download all the files specified in data/filenames:

enlace=$(cat data/urls)
for enlace_iterado in $enlace
do
        enlace_recortado=$(basename $enlace_iterado)
        if [ -f data/${enlace_recortado} ]
	then
	echo "Documento descargado previamente. No es necesario volver a descargarlo"
	else
	bash scripts/download.sh $enlace_iterado data
	fi
done

echo " "
echo "Genoma descargado."
echo " "

# Download the contaminants fasta file, and uncompress it:

if [ -f res/contaminants.fasta ]
then
echo "Secuencia de contaminantes descargada con anterioridad. No es necesario volver a realizarlo."
echo " "
else
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes #TO DO
echo "Fichero del genoma con contaminantes descargado."
echo " "
fi

## Debería descomprimirse desde el propio script download.sh

# Index the contaminants file:

if [ -d res/contaminants_idx ]
then
echo "El proceso de indexación ya se ha realizado con anterioridad."
echo " "
else
echo "Realizando proceso de indexación:"
mkdir -p res/contaminants_idx
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx
echo " "
echo "Indexación completada."
echo " "
fi

# Merge the samples into a single file:

echo "Realizando unión de archivos:"
echo " "

list_of_sample_ids=$(ls data/*.fastq.gz | cut -d "-" -f1 | sort | uniq)

for muestra in ${list_of_sample_ids} #TODO
do
	sid=$(basename ${muestra})
	if [ -f out/merged/${sid}.fastq.gz ]
	then
	echo "La unión de archivos se ha realizado con anterioridad. No es necesario repetirlo"
        echo " "
	else
	bash scripts/merge_fastqs.sh data out/merged $sid
        echo "==========> Muestra $sid unida."
        echo " "
	fi
done

echo "Unión realizada."
echo " "

# TO DO: run cutadapt for all merged files (se puede utilizar la siguiente línea pero modificándola):

echo "Realizando cutadapt para eliminar adaptadores:  "

for muestra in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sort | uniq) # TO DO
do
	echo " "
        sampleid=$(basename ${muestra})
        if [ -f log/cutadapt/${sampleid}.log ]
	then
	echo "El cutadapt se ha realizado previamente en la muestra ${sampleid}"
	else
	echo "Corriendo cutadapt para la muestra ${sampleid} :"
        cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sampleid}.trimmed.fastq.gz out/merged/${sampleid}.fastq.gz > log/cutadapt/${sampleid}.log
	fi
done

echo " "
echo "Eliminación de adaptadores completada."

# TO DO: run STAR for all trimmed files:

echo " "
echo "Realizando alineamiento:"

for fname in out/trimmed/*.fastq.gz
do

# you will need to obtain the sample ID from the filename:

        echo " "
        sampleid=$(basename $fname | cut -d "." -f1)
	buscar_archivo=$(basename $fname)
	if [ -d out/star/${sampleid} ]
	then
	echo "Ya se ha realizado previamente el STAR en la muestra ${sampleid}"
	else
        echo "Corriendo STAR para la muestra ${sampleid} :"
        mkdir -p out/star/$sampleid
        STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/$sampleid.trimmed.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix out/star/${sampleid}/
	fi
done

############################################################################################

## CREAR LOG:

# TO DO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run):

# - cutadapt: Reads with adapters and total basepairs:

if [ -f Log.out ]
then
echo " "
echo "El fichero de datos Log.out ya existe"
echo " "
else
	echo " "
	echo "Creando fichero Log.out para introducir los resultados de los análisis"
	echo " "
	nombres=$(ls log/cutadapt/*.log | cut -f 1 -d '.')
	for nombre_largo in $nombres
	do
        	nombre_log=$(basename $nombre_largo)
        	echo " " >> Log.out
        	echo "##############################################################" >> Log.out
        	echo "Información relacionada con $nombre_log según:" >> Log.out
        	echo " " >> Log.out
        	echo "cutadapt ==>" >> Log.out
        	grep "Reads with adapter" log/cutadapt/${nombre_log}.log >> Log.out
        	grep "Total basepairs processed" log/cutadapt/${nombre_log}.log >> Log.out
        	echo " " >> Log.out
        	echo "STAR ==>" >> Log.out
        	grep "Uniquely mapped reads %" out/star/${nombre_log}/Log.final.out >> Log.out
        	grep "% of reads mapped to multiple loci" out/star/${nombre_log}/Log.final.out >> Log.out
        	grep "% of reads mapped to too many loci" out/star/${nombre_log}/Log.final.out >> Log.out
        	echo " "
        	echo "##############################################################" >> Log.out
	done
fi

# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in (¡IMPORTANTE!)

# He añadido lo siguiente a lo anterior para que quede unificado para cada archivo.

##########################################################

echo "Alineamiento completado para todas las muestras."
echo " "
echo "Programa finalizado. Los resultados se pueden observar en el Log.out"
echo " "
echo "###############################################"
echo "########### LOG.OUT ==> RESULTADOS ############"
echo "###############################################"

cat Log.out
echo " "

##########################################################
