##########################################################
###### Script Para Indexitar Versión David 20-12-24 ######
################ Actualizado: 21-01-21 ###################
##########################################################

# INDICACIONES TOMÁS:

# This script should index the genome file specified in the first argument ($1),
# creating the index in a directory specified by the second argument ($2).

# The STAR command is provided for you. You should replace the parts surrounded by "<>" and uncomment it.

# COMENTARIOS MÍOS: He escrito 2 en lugar de 4 porque he comprobado que así falla menos (debido a que por falta de memoria,
# si el ordenador está ocupado con otra cosa puede acabar el proceso index.sh (Killed), pero no pasar con --runTHreadN 4 en
# en lugar de 2 (pasa menos).

# INDEXITACIÓN:

lista_archivos_index=$1
directorio_destino=$2

STAR --runThreadN 2 --runMode genomeGenerate --genomeDir $directorio_destino --genomeFastaFiles $lista_archivos_index --genomeSAindexNbases 9

##########################################################
