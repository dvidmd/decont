###############################################################
###### Script Para Realizar Merge Versión David 20-12-24 ######
################### Actualizado: 21-01-21 #####################
###############################################################

## INDICACIONES DE TOMÁS:

# This script should merge all files from a given sample (the sample id is provided in the third argument ($3))
# into a single file, which should be stored in the output directory specified by the second argument ($2).
# The directory containing the samples is indicated by the first argument ($1).

# COMENTARIO MÍOS:

# $1 son los archivos que hay que descargar
# $2 es el directorio donde hay que introducir los archivos descargados
# $3 es el sampleid, cuya condición indica si se descomprime si es == yes o no lo hace

# REALIZAR EL MERGE:

archivos_descargar=$1
directorio_destino=$2
sampleid=$3

cat $archivos_descargar/$sampleid* > $directorio_destino/$sampleid.fastq.gz

###############################################################
